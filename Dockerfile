FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
RUN apk add --no-cache bash
ENTRYPOINT ["java", "-jar","/app.jar"]

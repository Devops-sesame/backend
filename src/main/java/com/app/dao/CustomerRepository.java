package com.app.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.app.entities.Customer;


public interface CustomerRepository extends MongoRepository<Customer, String> {
   Customer findByMailAndPassword(String mail,String password);
}
